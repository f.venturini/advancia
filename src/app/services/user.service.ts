import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  user: any = {
  	username: '',
  	firstname: '',
  	lastname: '',
  	logged: false
  };

  constructor() { }

  login(username: string, password: string) {
  	console.log("Login successful");
  	this.user.username = username;
  	this.user.firstname = "Donald";
  	this.user.lastname = "Trump";
  	this.user.logged = true;
  }

  logout() {
  	console.log("Logut successful");
  	this.user.username = "";
  	this.user.firstname = "";
  	this.user.lastname = "";
  	this.user.logged = false;
  }

}
