import { Component, OnInit } from '@angular/core';

import { Validators, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;
	username: FormControl;
	password: FormControl;

  userSrv: UserService;

  constructor(builder: FormBuilder, userSrv: UserService, private router : Router) {
    this.userSrv = userSrv;
  	this.username = new FormControl('', [
  		Validators.required,
  		Validators.minLength(3)
  	]);
  	this.password = new FormControl('', [
  		Validators.required,
  		Validators.minLength(3)
  	]);
  	this.loginForm = builder.group({
      username: this.username,
      password: this.password
    });
  }

  ngOnInit() {
  }

  doLogin() {
  	console.log(this.loginForm.value);
    this.userSrv.login(this.loginForm.value.username, this.loginForm.value.password);

    this.router.navigate(['/']);
  }

  doLogout() {
    console.log('Addio' + this.loginForm.value);
    this.userSrv.logout();

    this.router.navigate(['/']);
  }

}
